<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Employee;

class Companies extends Model
{
    use HasFactory;

    public function hasEmployees()
    {
        return $this->hasMany(Employee::class);
    }
}
