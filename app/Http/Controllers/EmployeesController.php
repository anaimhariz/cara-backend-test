<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Employee;
use App\Models\Companies;
use Validator;

class EmployeesController extends Controller
{
    public function index()
    {
        $employee = Employee::with('user','company')->where('status',1)->get();
        return view('admin/employee/index',compact('employee'));
    }

    public function create()
    {
        $companies = Companies::where('status',1)->get();

        return view('admin/employee/add', compact('companies'));
    }

    public function store(Request $request)
    {
        $rules = [
            'fname'     => 'required',
            'lname'     => 'required',
            'company'   => 'required',
            'email'     => 'required|unique:users,email',
            'phone'     => 'required',
        ]; 
        
        $messages = [
            'fname.required'    => 'First Name is required',
            'lname.required'    => 'Last Name is required',
            'company.required'  => 'Company is required',
            'email.required'    => 'Email is required',
            'email.unique'      => 'Email is already registered',
            'phone.required'    => 'Phone is required',
        ];

        $validate =  Validator::make($request->all(),$rules,$messages);

        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate->messages())->withInput();
        }
        else
        {
            $fullname = $request->fname.' '.$request->lname;

            $user = new User;
            $user->name     = $fullname;
            $user->email    = $request->email;
            $user->password = bcrypt('password');
            $user->role     = 'employee';
            $user->save();

            $employee = new Employee;
            $employee->first_name    = $request->fname;
            $employee->last_name     = $request->lname;
            $employee->user_id       = $user->id;
            $employee->company_id    = $request->company;
            $employee->phone         = $request->phone;
            $employee->save();

            return redirect()->route('employee')->with('status', 'New Employee Successfully Added!');
        }
    }

    public function edit($id)
    {
        $employee = Employee::find($id);
        $companies = Companies::where('status',1)->get();

        return view('admin/employee/edit', compact('employee','companies'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'fname'     => 'required',
            'lname'     => 'required',
            'company'   => 'required',
            'email'     => 'required|unique:users,email',
            'phone'     => 'required',
        ]; 
        
        $messages = [
            'fname.required'    => 'First Name is required',
            'lname.required'    => 'Last Name is required',
            'company.required'  => 'Company is required',
            'email.required'    => 'Email is required',
            'email.unique'      => 'Email is already registered',
            'phone.required'    => 'Phone is required',
        ];

        $validate =  Validator::make($request->all(),$rules,$messages);

        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate->messages())->withInput();
        }
        else
        {
            $fullname = $request->fname.' '.$request->lname;

            $employee = Employee::find($id);
            $employee->first_name    = $request->fname;
            $employee->last_name     = $request->lname;
            $employee->company_id    = $request->company;
            $employee->phone         = $request->phone;
            $employee->save();

            $user = User::find($employee->user_id);
            $user->name     = $fullname;
            $user->email    = $request->email;
            $user->save();

            return redirect()->route('employee')->with('status', 'Edit Employee Successfully Added!');
        }
    }

    public function delete($id)
    {
        $employee = Employee::find($id);
        $employee->status    = 0;
        $employee->save();

        $user = User::find($employee->user_id);
        $user->status     = 0;
        $user->save();

        return redirect()->route('employee')->with('status', 'Delete Employee Successfully Added!');
    }
}
