<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Companies;
use App\Models\Employee;
use Validator;

class CompaniesController extends Controller
{
    public function index()
    {
        $companies = Companies::where('status',1)->get();
        return view('admin/company/list',compact('companies'));
    }

    public function create()
    {
        return view('admin/company/add');
    }

    public function store(Request $request)
    {
        $rules = [
            'name'      => 'required',
            'email'     => 'required|unique:companies,email',
            'logo'      => 'required|mimes:jpg,png,jpeg|dimensions:min_width=100,min_height=100',
            'website'   => 'required',
        ]; 
        
        $messages = [
            'name.required'     => 'Name is required',
            'email.required'    => 'Email is required',
            'email.unique'      => 'Email is already registered',
            'logo.mimes'        => 'File must be JPG,JPEG and PNG and minimum 100x100',
            'logo.dimensions'   => 'File must be JPG,JPEG and PNG and minimum 100x100',
            'website.required'  => 'Website is required',
        ];

        $validate =  Validator::make($request->all(),$rules,$messages);

        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate->messages())->withInput();
        }
        else
        {
            $logoName = $request->logo->getClientOriginalName();
            $filePath = $logoName;

            $path = Storage::disk('public')->put($filePath, file_get_contents($request->logo));
            $path = Storage::disk('public')->url($path);

            $company = new Companies;
            $company->name      = $request->name;
            $company->email     = $request->email;
            $company->logo      = $logoName;
            $company->website   = $request->website;
            $company->save();

            return redirect()->route('companies')->with('status', 'New Company Successfully Added!');
        }
    }

    public function edit($id)
    {
        $company = Companies::find($id);

        return view('admin/company/edit', compact('company'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name'      => 'required',
            'email'     => 'required|unique:companies,email',
            'logo'      => 'required|mimes:jpg,png,jpeg|dimensions:min_width=100,min_height=100',
            'website'   => 'required',
        ]; 
        
        $messages = [
            'name.required'     => 'Name is required',
            'email.required'    => 'Email is required',
            'email.unique'      => 'Email is already registered',
            'logo.mimes'        => 'File must be JPG,JPEG and PNG and minimum 100x100',
            'logo.dimensions'   => 'File must be JPG,JPEG and PNG and minimum 100x100',
            'website.required'  => 'Website is required',
        ];

        $validate =  Validator::make($request->all(),$rules,$messages);

        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate->messages())->withInput();
        }
        else
        {
            
            $logoName = $request->logo->getClientOriginalName();
            $filePath = $logoName;

            $path = Storage::disk('public')->put($filePath, file_get_contents($request->logo));
            $path = Storage::disk('public')->url($path);

            $company            = Companies::find($id);
            $company->name      = $request->name;
            $company->email     = $request->email;
            $company->logo      = $logoName;
            $company->website   = $request->website;
            $company->update();

            return redirect()->route('companies')->with('status', 'Company Successfully Updated!');
        }
    }

    public function delete($id)
    {
        Employee::where('company_id',$id)->update(['company_id' => null]);
        
        $company = Companies::find($id);
        $company->status    = 0;
        $company->save();

        return redirect()->route('companies')->with('status', 'Delete Company Successfully Added!');
    }
}
