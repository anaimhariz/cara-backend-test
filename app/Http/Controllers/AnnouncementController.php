<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Models\Announcement;
use App\Models\User;
use Validator;

class AnnouncementController extends Controller
{
    public function index()
    {
        $announcement = Announcement::where('status',1)->get();
        return view('admin/announcement/index',compact('announcement'));
    }

    public function create()
    {
        return view('admin/announcement/add');
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'desc'  => 'required',
            'image' => 'required|mimes:jpg,png,jpeg',
        ]; 
        
        $messages = [
            'title.required'=> 'Title is required',
            'desc.required' => 'Description is required',
            'image.mimes'   => 'File must be JPG,JPEG and PNG',
        ];

        $validate =  Validator::make($request->all(),$rules,$messages);

        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate->messages())->withInput();
        }
        else
        {
            $imageName = $request->image->getClientOriginalName();
            $filePath = $imageName;

            $path = Storage::disk('public')->put($filePath, file_get_contents($request->image));
            $path = Storage::disk('public')->url($path);

            $announcement = new Announcement;
            $announcement->title        = $request->title;
            $announcement->description  = $request->desc;
            $announcement->image        = $imageName;
            $announcement->save();

            $details = [
                'title'         => $request->title,
                'pathToImage'   => url('storage/'.$imageName),
                'body'          => $request->desc
            ];

            $employees = User::where('role','employee')->get();
            foreach ($employees as $employee) {
                Mail::to($employee->email)->send(new \App\Mail\MyTestMail($details));
            }

            return redirect()->route('announcement')->with('status', 'New Announcement Successfully Added and Email Has Been Send!');
        }
    }

    public function edit($id)
    {
        $announcement = Announcement::find($id);

        return view('admin/announcement/edit', compact('announcement'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'desc'  => 'required',
            'image' => 'required|mimes:jpg,png,jpeg',
        ]; 
        
        $messages = [
            'title.required'=> 'Title is required',
            'desc.required' => 'Description is required',
            'image.mimes'   => 'File must be JPG,JPEG and PNG',
        ];

        $validate =  Validator::make($request->all(),$rules,$messages);

        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate->messages())->withInput();
        }
        else
        {
            
            $imageName = $request->image->getClientOriginalName();
            $filePath = $imageName;

            $path = Storage::disk('public')->put($filePath, file_get_contents($request->image));
            $path = Storage::disk('public')->url($path);

            $announcement               = Announcement::find($id);
            $announcement->title        = $request->title;
            $announcement->description  = $request->desc;
            $announcement->image        = $imageName;
            $announcement->update();

            return redirect()->route('announcement')->with('status', 'Announcement Successfully Updated!');
        }
    }

    public function delete($id)
    {
        $announcement = Announcement::find($id);
        $announcement->status = 0;
        $announcement->save();

        return redirect()->route('announcement')->with('status', 'Delete Announcement Successfully Added!');
    }
}
