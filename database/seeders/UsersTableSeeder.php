<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['id' => 1, 'name' => 'Administrator', 'email' => 'admin@admin.com', 'password' => bcrypt('password'), 'role' => 'admin', 'status' => '1'],
        ];

        User::insert($users);
    }
}
