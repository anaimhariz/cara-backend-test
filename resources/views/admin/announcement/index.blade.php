@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if(session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        List Announcement
                        <button type="button" id="add-announcement" class="btn btn-outline-primary float-right" onclick="window.location='{{ route("announcement.add") }}'"><i class="fa fa-plus-square-o"></i> Announcement</button>
                    </div>
                </div>
                @foreach($announcement as $key => $data)
                    <div class="card">
                        <img class="card-img-top" src="{{ url('storage/'.$data->image) }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{ $data->title }}</h5>
                            <p class="card-text">{{ $data->description }}</p>
                            <form action="{{ route('announcement.delete',$data->id) }}" method="POST" >
                                @csrf
                                <button type="submit" class="btn btn-danger float-right" onclick="return confirm('Are you sure you want to DELETE this?')">Delete</button>
                                <a href="{{ route('announcement.edit', $data->id) }}" class="btn btn-primary float-right">Edit</a>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection
