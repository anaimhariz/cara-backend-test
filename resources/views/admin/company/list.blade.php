@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if(session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        List Company
                        <button type="button" id="add-company" class="btn btn-outline-primary float-right" onclick="window.location='{{ route("companies.add") }}'"><i class="fa fa-plus-square-o"></i> Company</button>
                    </div>
                </div>

                <div class="card-body">
                <table id="daTable" class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Logo</th>
                            <th scope="col">Website</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($companies as $key => $data)
                            <tr>
                                <td class="align-middle">{{ $key+1 }}</td>
                                <td class="align-middle">{{ $data->name }}</td>
                                <td class="align-middle">{{ $data->email }}</td>
                                <td class="align-middle"><img target="_blank" src="{{ url('storage/'.$data->logo) }}" alt="" title="" width="80px" height="80px"/></td>
                                <td class="align-middle"><a href='http://{{ $data->website }}'>{{ $data->website }}</a></td>
                                <td class="align-middle">
                                    <form action="{{ route('companies.delete',$data->id) }}" method="POST" >
                                    @csrf
                                        <div class="btn-group" role="group">
                                            <a type="button" class="btn btn-primary" href="{{ route('companies.edit', $data->id) }}" role="button">Edit</a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to DELETE this? If you delete this some function will cannot run properly')">Delete</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection
