@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if(session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        List Employee
                        <button type="button" id="add-employee" class="btn btn-outline-primary float-right" onclick="window.location='{{ route("employee.add") }}'"><i class="fa fa-plus-square-o"></i> Employee</button>
                    </div>
                </div>

                <div class="card-body">
                <table id="daTable" class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Company</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($employee as $key => $data)
                            <tr>
                                <td class="align-middle">{{ $key+1 }}</td>
                                <td class="align-middle">{{ $data->first_name }}</td>
                                <td class="align-middle">{{ $data->last_name }}</td>
                                <td class="align-middle">{{ $data->company->name ?? 'No Company' }}</td>
                                <td class="align-middle">{{ $data->user->email }}</td>
                                <td class="align-middle">{{ $data->phone }}</td>
                                <td class="align-middle">
                                    <form action="{{ route('employee.delete',$data->id) }}" method="POST" >
                                    @csrf
                                        <div class="btn-group" role="group">
                                            <a type="button" class="btn btn-primary" href="{{ route('employee.edit', $data->id) }}" role="button">Edit</a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to DELETE this?')" role="button">Delete</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection
