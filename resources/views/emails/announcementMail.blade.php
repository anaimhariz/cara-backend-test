<!DOCTYPE html>
<html>
<head>
    <title>Online Web Tutor</title>
</head>
<body>
  
    <h1>{{ $details['title'] }}</h1>
    <img src="{{ $details['pathToImage]' }}">
    <p>{{ $details['body'] }}</p>
   
    <p>Thank you</p>
</body>
</html>