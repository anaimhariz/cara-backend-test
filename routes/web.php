<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::match(['get', 'post'], 'register', function()
{
    return redirect('/login');
});

Route::middleware(['role:admin'])->group(function () {
    Route::get('/admin/home', [App\Http\Controllers\HomeController::class, 'indexAdmin'])->name('homeAdmin');

    //company
    Route::get('/admin/companies', [App\Http\Controllers\CompaniesController::class, 'index'])->name('companies');
    Route::get('/admin/companies/add', [App\Http\Controllers\CompaniesController::class, 'create'])->name('companies.add');
    Route::post('/admin/companies/store', [App\Http\Controllers\CompaniesController::class, 'store'])->name('companies.store');
    Route::get('/admin/companies/edit/{id}', [App\Http\Controllers\CompaniesController::class, 'edit'])->name('companies.edit');
    Route::put('/admin/companies/update/{id}', [App\Http\Controllers\CompaniesController::class, 'update'])->name('companies.update');
    Route::post('/admin/companies/delete/{id}', [App\Http\Controllers\CompaniesController::class, 'delete'])->name('companies.delete');

    //employee
    Route::get('/admin/employees', [App\Http\Controllers\EmployeesController::class, 'index'])->name('employee');
    Route::get('/admin/employees/add', [App\Http\Controllers\EmployeesController::class, 'create'])->name('employee.add');
    Route::post('/admin/employees/store', [App\Http\Controllers\EmployeesController::class, 'store'])->name('employee.store');
    Route::get('/admin/employees/edit/{id}', [App\Http\Controllers\EmployeesController::class, 'edit'])->name('employee.edit');
    Route::put('/admin/employees/update/{id}', [App\Http\Controllers\EmployeesController::class, 'update'])->name('employee.update');
    Route::post('/admin/employees/delete/{id}', [App\Http\Controllers\EmployeesController::class, 'delete'])->name('employee.delete');

    //announcement
    Route::get('/admin/announcement', [App\Http\Controllers\AnnouncementController::class, 'index'])->name('announcement');
    Route::get('/admin/announcement/add', [App\Http\Controllers\AnnouncementController::class, 'create'])->name('announcement.add');
    Route::post('/admin/announcement/store', [App\Http\Controllers\AnnouncementController::class, 'store'])->name('announcement.store');
    Route::get('/admin/announcement/edit/{id}', [App\Http\Controllers\AnnouncementController::class, 'edit'])->name('announcement.edit');
    Route::put('/admin/announcement/update/{id}', [App\Http\Controllers\AnnouncementController::class, 'update'])->name('announcement.update');
    Route::post('/admin/announcement/delete/{id}', [App\Http\Controllers\AnnouncementController::class, 'delete'])->name('announcement.delete');
});

Route::middleware(['role:employee'])->group(function () {
    Route::get('/employee/home', [App\Http\Controllers\HomeController::class, 'indexEmployee'])->name('homeEmployee');
});

